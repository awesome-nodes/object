/* eslint-disable @typescript-eslint/ban-types */
import { AbstractClass } from 'object/AbstractClass';
import { ConstructorFor, ConstructorFunction, Nullable } from 'simplytyped';


export function isPrototypeOf<T extends object>(
    constructor: Nullable<T | ConstructorFor<T>>,
    prototype: ConstructorFor<object>): constructor is T | ConstructorFor<T>
{
    if (constructor == prototype || constructor instanceof prototype)
        return true;
    const prototypeOf = Object.getPrototypeOf(constructor);
    if (prototypeOf)
        return isPrototypeOf(prototypeOf, prototype);

    return false;
}

/**
 * Returns 'true' if a value is a simple function but not a constructor.
 * @param {Function} value
 * @returns {value is ConstructorFunction<T>}
 */
export function isFunction(value: unknown): value is () => unknown
{
    return (value && typeof value === 'function' && !isConstructor(value as AbstractClass<unknown>)) as boolean;
}

/**
 * Returns 'true' if a value is an instantiable object type but not a simple function.
 * @param {Function} value
 * @returns {value is ConstructorFunction<T>}
 */
export function isConstructor<T extends AbstractClass<object>>(value: unknown): value is ConstructorFunction<T>
{
    return (value && typeof value === 'function' && value.prototype && value.prototype.constructor) === value;
}

/**
 * Returns 'true' if a value is an object.
 * @param value
 * @returns {value is {}}
 */
export function isObject(value: unknown): value is {}
{
    return (value && typeof value === 'object' && value!.constructor === Object) as boolean;
}

/**
 * Returns 'true' if a value is an array.
 * @param value
 * @returns {value is Array<T>}
 */
export function isArray<T>(value: unknown): value is Array<T>
{
    return (value && typeof value === 'object' && value!.constructor === Array) as boolean;
}

/**
 * Returns 'true' if a value is a string.
 * @param value
 * @returns {value is string}
 */
export function isString(value: unknown): value is string
{
    return typeof value === 'string' || value instanceof String;
}

/**
 * Returns 'true' if a value is really a number.
 * @param value
 * @returns {value is number}
 */
export function isNumber(value: unknown): value is number
{
    return typeof value === 'number' && isFinite(value);
}

/**
 * Returns 'true' if a value is a regexp.
 * @param value
 * @returns {value is RegExp}
 */
export function isRegExp(value: unknown): value is RegExp
{
    return (value && typeof value === 'object' && value!.constructor === RegExp) as boolean;
}

/**
 * Returns 'true' if value is an error object.
 * @param value
 * @returns {value is Error}
 */
export function isError(value: unknown): value is Error
{
    return value instanceof Error && typeof value.message !== 'undefined';
}

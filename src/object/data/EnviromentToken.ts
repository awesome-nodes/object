import { InjectionScope, InjectionToken } from '@awesome-nodes/injection-factory';
import { Nullable } from 'simplytyped';


/**
 * Defines the application environment used to determine if the application is whether in production (**true**),
 * development (**false**) or in design/test mode (**null** / **undefined**).
 * @type {InjectionToken<Nullable<boolean>>}
 */
export const ENVIRONMENT_TOKEN: InjectionToken<Nullable<boolean>> = InjectionScope
    .get(Symbol('ENVIRONMENT'), null)
    .AddToken('ENVIRONMENT_TOKEN');

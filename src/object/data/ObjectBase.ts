import { ObjectBase as Base } from 'object';
import { toJSON } from 'object/data/JSON';
import { PlainObject, StringKeys } from 'simplytyped';


export class ObjectBase extends Base
{
    readonly #_toJSON: typeof toJSON;

    /**
     * Initializes this instance.
     * @param {string} name If omitted, the class name of this object is used as default object name.
     * @exception ArgumentNullException Thrown if an empty object name was provided.
     */
    public constructor(name?: string)
    {
        super(name);

        this.#_toJSON = toJSON;
    }

    /**
     * Converts this instance to a JSON string, optionally including only
     * the specified properties if a set of property names is specified.
     * @param {StringKeys<this | T>} includeProperties A list of property names.
     * @returns {PlainObject}
     */
    //eslint-disable-next-line @typescript-eslint/no-unused-vars
    public toJSON<T = this>(...includeProperties: Array<StringKeys<T | this>>): PlainObject
    {
        return this.#_toJSON(...includeProperties);
    }
}

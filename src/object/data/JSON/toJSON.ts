/**
 * Converts this instance to a JSON string, optionally including only
 * the specified properties if a set of property names is specified.
 * @param {StringKeys<this | T>} includeProperties
 * @returns {PlainObject}
 */
import { isArray } from 'object';
import { ENVIRONMENT_TOKEN } from 'object/data/EnviromentToken';
import { getAllPropertyNames, getPropertyDescriptor } from 'object/reflection/getPropertyDescriptor';
import { PlainObject, StringKeys } from 'simplytyped';


/**
 * Converts this instance to a JSON string, optionally including only
 * the specified properties if a set of property names is specified.
 * @param {StringKeys<this | T>} includeProperties A list of property names.
 * @returns {PlainObject}
 */
export function toJSON<T>(this: PlainObject, ...includeProperties: Array<StringKeys<T>>): PlainObject
{
    const production = ENVIRONMENT_TOKEN.Value;

    includeProperties      = includeProperties.filter(p => p != '');
    const jsonEntries      = new Map<string, unknown>();
    const proto            = Object.getPrototypeOf(this);
    const allPropertyNames = getAllPropertyNames(proto);
    for (const key of allPropertyNames) {
        const desc                 = getPropertyDescriptor(proto, key);
        const hasGetter            = desc && typeof desc.get === 'function';
        const includePropertyIndex = (includeProperties as Array<string>).indexOf(key);
        const include              = includePropertyIndex > -1;
        if (hasGetter && (includeProperties.length == 0 || include)) {

            const value = (this as PlainObject)[key] instanceof Map
                ? Array.from((this as PlainObject)[key].values())
                : (this as PlainObject)[key];

            if (value instanceof Array && !value.length ||
                production && key == 'Name' && value == this.constructor.name)
                continue;

            const jsonKey = key[0].toLowerCase() + key.substr(1);
            if (include)
                (includeProperties as Array<string>)[includePropertyIndex] = jsonKey;

            jsonEntries.set(jsonKey, value == null
                ? value
                : typeof value == 'object' && 'toJSON' in value
                    ? value.toJSON()
                    : isArray(value)
                        ? value.map(item => item && typeof item === 'object' && 'toJSON' in item
                            ? (item as PlainObject).toJSON()
                            : item)
                        : value);
        }
    }

    return includeProperties.length == 0
        ? Array.from(jsonEntries.entries()).reduce((main, [key, value]) => ({ ...main, [key]: value }), {})
        : (includeProperties as Array<string>).reduce(
            (current, next) =>
            {
                (current as PlainObject)[next] = jsonEntries.get(next);

                return current;
            },
            {},
        );
}

import { PlainObject } from 'simplytyped';


declare global
{
    //tslint:disable-next-line:interface-name
    interface JSON
    {
        /**
         * Converts this instance to a JSON string, optionally including only
         * the specified properties if a set of property names is specified.
         * @param {PlainObject} obj
         * @param {number} indent
         * @returns {string}
         */
        safeStringify(obj: PlainObject, indent?: number): string;
    }
}

JSON.safeStringify = (obj: PlainObject, indent = 2): string =>
{
    const cache = new WeakSet();

    return JSON.stringify(
        obj,
        (key, value) =>
        {
            return typeof value === 'object' && value != null
                ? cache.has(value)
                    ? undefined // Duplicate reference found, discard key
                    : cache.add(value) && value // Store value in our collection)
                : value;
        },
        indent,
    );
};

import { ENVIRONMENT_TOKEN } from 'object/data/EnviromentToken';
import { ObjectBase } from 'object/data/ObjectBase';


describe('ObjectBase', () =>
{
    beforeAll(() =>
    {
        ENVIRONMENT_TOKEN.Scope.AddProvider({
            provide : ENVIRONMENT_TOKEN,
            useValue: false,
        });
    });

    it('should convert to JSON', (() =>
    {
        interface JsonTest
        {
            PropertyA: string,
            PropertyB: string,
        }

        const jsonTestCLass = class extends ObjectBase implements JsonTest
        {
            private readonly _PropertyA: string;
            private readonly _PropertyB: string;
            private readonly _PropertyC: string;

            public get PropertyA(): string
            {
                return this._PropertyA;
            }

            public get PropertyB(): string
            {
                return this._PropertyB;
            }

            public get PropertyC(): string
            {
                return this._PropertyC;
            }

            public constructor(jsonData: JsonTest)
            {
                super();
                this._PropertyA = jsonData.PropertyA;
                this._PropertyB = jsonData.PropertyB;
                this._PropertyC = 'PropertyC';
            }

            public toJSON(): JsonTest
            {
                return super.toJSON('PropertyA', 'PropertyB') as JsonTest;
            }
        };

        const testClass  = new jsonTestCLass({
            PropertyA: 'PropertyA',
            PropertyB: 'PropertyB',
        });
        const objectJson = JSON.safeStringify(testClass, 0);

        expect(objectJson).toEqual('{"propertyA":"PropertyA","propertyB":"PropertyB"}');
    }));
});

import { Enum } from 'object/Enum';
import { PlainObject } from 'simplytyped';


/**
 * Provides an enumeration that can be treated as a bit field; that is, a set of flags.
 */
export abstract class FlagsEnum extends Enum<number>
{
    private readonly _Flags = new Array<this>();

    /**
     * Returns the enum flags of this enumeration instance if there are any.
     * @returns {Array<this>}
     * @constructor
     */
    public get Flags(): Array<this>
    {
        return this._Flags;
    }

    /**
     * Initializes this flags enumeration instance.
     * @param {number} value The value associated with this enumeration member instance.
     * @param {string} name An optional name to be used as named enumeration identifier.
     * Note: You can only use flags which having their static enum property already initialized.
     */
    protected constructor(value: number, name?: string)
    {
        super(value, name);

        const thisType = this.constructor as typeof FlagsEnum;
        const _value   = thisType.Parse(value);
        if (!_value) {
            // Iterate over all static fields
            for (const _property in thisType) {
                //eslint-disable-next-line no-prototype-builtins
                if (!thisType.hasOwnProperty(_property))
                    continue;

                // Retrieve property value
                const enumMember = (thisType as PlainObject)[_property];

                if (enumMember instanceof thisType) {
                    // Lookup if the enumeration value is specified within this flags enum.
                    //tslint:disable-next-line:no-bitwise
                    if ((value & enumMember.Value) == enumMember.Value
                        && !this._Flags.some(f => f == enumMember))
                        (this._Flags as PlainObject)[_property] = enumMember;
                }
            }
        }
    }

    /**
     * Returns a boolean value which indicates if this enumeration instance contains the provided enum flag.
     * @param {this} flag
     * @returns {boolean}
     * @constructor
     */
    public HasFlag(flag: this): boolean
    {
        return this._Flags.some(f => f == flag);
    }

    public toString(): string
    {
        return this.Flags.length > 0
            ? `${this.Name}(${this._Flags.filter(item => item != null).map(item => item.Value).join('|')})`
            : super.toString();
    }
}

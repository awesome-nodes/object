import { Exception } from 'object/Exception';
import { TemplateString } from 'object/TemplateString';
import { Nullable } from 'simplytyped';


export class ArgumentException extends Exception
{
    public get ParamName(): Nullable<string>
    {
        return this._ParamName;
    }

    public constructor(
        message: TemplateString<string>, private readonly _ParamName?: string, innerException?: Exception)
    {
        super(message, { paramName: _ParamName }, innerException);
    }

    public toString(): string
    {
        let toString = super.toString();
        if (this._ParamName && toString.indexOf(this._ParamName) == -1)
            toString += `\nParameter Name: ${this._ParamName}`;

        return toString;
    }
}

/**
 * Defines a generalized method that a value type or class implements
 * to create a type-specific method for determining equality of instances.
 * @description "If it walks like a duck and it quacks like a duck, then it must be a duck."
 * @see https://docs.microsoft.com/de-de/dotnet/api/system.iequatable-1
 */
export interface IEquatable<T extends object>
{
    /**
     * Determines if this instance equals the provided other instance of the same type.
     * @param {T} other An object to compare with this object.
     * @returns {boolean} `true` if the current object is equal to the other parameter; otherwise, `false`.
     */
    Equals(other: T): boolean;
}

/**
 * Determines if the provided object implements the {@link IEquatable} interface.
 * @param {object} instance
 * @returns {instance is IEquatable<T>} `true` if the provided object implements the {@link IEquatable} interface;
 * otherwise, `false`.
 */
export function isEquitable<T extends object>(instance: object): instance is IEquatable<T>
{
    return 'Equals' in instance;
}

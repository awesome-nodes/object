import { ArgumentNullException } from 'object/ArgumentNullException';
import { IEquatable } from 'object/IEquatable';
import { isString } from 'object/TypeChecks';


export class ObjectBase implements Object, IEquatable<ObjectBase>
{
    protected _Name: string;

    /**
     * Returns the name of this object instance.
     * @returns {string}
     */
    public get Name(): string
    {
        return this._Name;
    }

    /**
     * Sets the name of this object instance.
     * @param {string} value
     */
    public set Name(value: string)
    {
        this._Name = value;
    }

    /**
     * Initializes this instance.
     * @param {string} name If omitted, the class name of this object is used as default object name.
     * @exception ArgumentNullException Thrown if an empty object name was provided.
     */
    public constructor(name?: string)
    {
        if (isString(name) && !name.length)
            throw new ArgumentNullException('Invalid object name. ', 'name');

        this._Name = name || ObjectBase.ToString(this);
    }

    /** @inheritDoc */
    public toString(): string
    {
        return this._Name;
    }

    /**
     * Checks equality by comparing the string representation of this type.
     * @inheritDoc
     * @param other
     */
    public Equals(other: ObjectBase): boolean
    {
        // Check self
        if (other == this)
            return true;

        // Check for null
        if (other == null)
            return false;

        // Check type
        if (!(other instanceof this.constructor))
            return false;

        // Check member
        return this.toString() == other.toString();
    }

    /**
     * Returns a string representation of any object.
     * @param object
     * @returns {string}
     */
    public static ToString(object: unknown): string
    {
        //noinspection FallThroughInSwitchStatementJS
        switch (typeof object) {
            case 'function':
                return object.name;
            case 'object':
                if (object)
                    return object.constructor.name;
            //tslint:disable-next-line:no-switch-case-fall-through
            default:
                return String(object);
        }
    }
}

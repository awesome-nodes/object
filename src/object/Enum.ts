import { ArgumentException } from 'object/ArgumentException';
import { ArgumentNullException } from 'object/ArgumentNullException';
import { ObjectBase } from 'object/ObjectBase';
import { isNumber, isString } from 'object/TypeChecks';
import { ConstructorFor, Nullable, PlainObject } from 'simplytyped';


export type EnumValueType = boolean | PropertyKey;

/**
 * Provides a base class for enumerations.
 */
export abstract class Enum<TValue extends EnumValueType> extends ObjectBase
{
    public get Value(): TValue
    {
        return this._Value;
    }

    /**
     * Initializes this enumeration instance.
     * @param {TValue} _Value The value associated with this enumeration member instance.
     * @param {string} name An optional name to be used as named enumeration identifier.
     */
    protected constructor(private readonly _Value: TValue, name?: string)
    {
        super(name);

        Enum.CheckValue(_Value);
    }

    /**
     * @inheritDoc
     * Returns the enumeration value unless an optional identifier name was specified within the constructor.
     */
    public toString(): string
    {
        return this.Name != ObjectBase.ToString(this) ? this.Name : String(this._Value);
    }

    protected static CheckValue<T extends EnumValueType>(value: T): boolean
    {
        const type = value != null ? typeof value : String(value);
        switch (type) {
            case 'boolean':
            case 'symbol':
                return true;
            default:
                if (isNumber(value) || isString(value) && value.length)
                    return true;
                if ((isString(value) && !value.length) || value == 'null' || value == 'undefined')
                    throw new ArgumentNullException('Enum members must have a value. ', 'value');

                throw new ArgumentException(
                    `The provided enum value of type '${type}' for enum '${ObjectBase.ToString(this)
                    }' is invalid. Only the types 'boolean', 'number', 'string' and 'symbol' are allowed.`,
                    'value');
        }
    }

    /**
     * Creates a static get property for the specified enum member
     * which will override the enum property in the derived class.
     * @param {TValue} value The enumeration value.
     * @param {string} name An optional name to be used as named enumeration identifier.
     * @returns {U} The enum instance.
     */
    public static Enum<U extends Enum<TValue>, TValue extends EnumValueType = EnumValueType>(
        value: TValue, name?: string): U
    {
        this.CheckValue(value);

        const instance: U = new (this as unknown as ConstructorFor<U>)(value, name);

        Object.defineProperty(
            this,
            typeof instance._Value == 'boolean' ? Number(instance._Value) : instance._Value as PropertyKey,
            {
                get         : () =>
                {

                    return instance;
                },
                set         : () =>
                {
                },
                enumerable  : true,
                configurable: true,
            },
        );

        return instance;
    }

    /**
     * Creates a static get string property only for the specified enum member value
     * which will override the enum property in the derived class.
     * @param value
     * @returns {string} The enum value.
     */
    public static Value<T extends EnumValueType>(value: T): T
    {
        this.CheckValue(value);

        Object.defineProperty(this, String(value), {
            get         : () =>
            {
                return value;
            },
            set         : () =>
            {
            },
            enumerable  : true,
            configurable: true,
        });

        return value;
    }

    /**
     * Parses the value to Enum by returning the enum instance or the value itself depending on the enum type.
     * @param {TValue} value
     * @returns {Nullable<U>}
     */
    public static Parse<U extends Enum<TValue>, TValue extends EnumValueType = EnumValueType>(
        value: TValue,
    ): Nullable<U>
    {
        // Iterate through all static properties of this Enum type.
        for (const _field in this) {
            if (!Object.hasOwnProperty.call(this, _field))
                continue;

            // Retrieve property value
            const propertyValue = (this as PlainObject)[_field];

            // If property value is type of Enum then compare its value
            if ((propertyValue instanceof this && propertyValue.Value == value)
                // Otherwise compare both values directly
                || propertyValue == value)
                return propertyValue;
        }

        return null;
    }
}

import { TemplateString, TemplateStringValues } from 'object/TemplateString';
import { isString } from 'object/TypeChecks';
import { Nullable } from 'simplytyped';


/**
 * Represents the base class for every exception thrown from within the object model.
 * Derive from this class to create exception domains to detect exception origin locations.
 */
export class Exception extends Error
{
    //region Public Properties

    /**
     * Returns a human-readable description of the error.
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error/message
     * @returns {string}
     */
    public get Message(): string
    {
        return this.message;
    }

    /**
     * Returns an {@link Error} or {@link Exception} instance which caused this exception.
     * @returns {Error}
     */
    public get Parent(): Nullable<Error>
    {
        return this._InnerException;
    }

    /**
     * The non-standard stack property of Error objects offer a trace of which functions were called, in what order,
     * from which line and file, and with what arguments. The stack string proceeds from the most recent calls to
     * earlier ones, leading back to the original global scope call.
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error/stack
     * @returns {string}
     */
    public get StackTrace(): Nullable<string>
    {
        return this.stack;
    }

    //endregion

    /**
     * Initializes this instance.
     * Important Note: When deriving from {@link Exception} always call `setPrototype` in the constructor of every
     * derived exception type to enable `instanceof` type checking.
     * @param {string} message The error message
     * @param {TemplateStringValues} messageTemplateValues A list of message string template interpolation values.
     * @param {Error} _InnerException? An
     * @constructor
     */
    public constructor(
        message: TemplateString,
        messageTemplateValues: TemplateStringValues = {},
        private readonly _InnerException?: Error)
    {
        super(isString(message) ? message : message({
            ...messageTemplateValues,
            parent: _InnerException,
        }));

        Exception.setPrototype(this);
    }

    /** @inheritDoc */
    public toString(): string
    {
        return `${this.constructor.name}: ${this.message}`;
    }

    /**
     * Sets the prototype explicitly for the calling type.
     * Call this method always from within derived instances to set the exception prototype to this type.
     * @param {Exception} instance
     */
    protected static setPrototype(instance: Exception): void
    {
        Object.setPrototypeOf(instance, this.prototype);
    }
}

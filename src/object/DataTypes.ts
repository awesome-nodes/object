import { Enum, EnumValueType } from 'object/Enum';
import {
    isArray,
    isConstructor,
    isError,
    isFunction,
    isNumber,
    isObject,
    isRegExp,
    isString,
} from 'object/TypeChecks';
import { Nullable } from 'simplytyped';


/**
 * Represents a set of primitive value type and object type enumerations which can be parsed from any unknown type.
 */
export class DataTypes extends Enum<string>
{
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean
     */
    public static Boolean: DataTypes   = DataTypes.Enum('Boolean');
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String
     */
    public static Number: DataTypes    = DataTypes.Enum('String');
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/BigInt
     */
    public static BigInt: DataTypes    = DataTypes.Enum('Bigint');
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String
     */
    public static String: DataTypes    = DataTypes.Enum('String');
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Symbol
     */
    public static Symbol: DataTypes    = DataTypes.Enum('Symbol');
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date
     */
    public static Date: DataTypes      = DataTypes.Enum('Date');
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp
     */
    public static RegExp: DataTypes    = DataTypes.Enum('RegExp');
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array
     */
    public static Array: DataTypes     = DataTypes.Enum('Array');
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function
     */
    public static Function: DataTypes  = DataTypes.Enum('Function');
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object
     */
    public static Object: DataTypes    = DataTypes.Enum('Object');
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes/constructor
     */
    public static Type: DataTypes      = DataTypes.Enum('Type');
    /**
     * @see https://developer.mozilla.org/en-US/docs/Glossary/Instance
     */
    public static Instance: DataTypes  = DataTypes.Enum('Instance');
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Null
     */
    public static Null: DataTypes      = DataTypes.Enum('null');
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Undefined
     */
    public static Undefined: DataTypes = DataTypes.Enum('undefined');
    /**
     * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error
     */
    public static Error: DataTypes     = DataTypes.Enum('Error');

    public static Parse<U extends Enum<TValue>, TValue extends EnumValueType>(value: unknown): Nullable<U>
    {
        let type: Nullable<DataTypes> = null;
        switch (value != null ? typeof value : String(value)) {
            case 'boolean':
                type = this.Boolean;
                break;
            case 'bigint':
                type = this.BigInt;
                break;
            case 'symbol':
                type = this.Symbol;
                break;
            case 'null':
                type = this.Null;
                break;
            case 'undefined':
                type = this.Undefined;
                break;
            default:
                if (isNumber(value))
                    type = this.Number;
                else if (isString(value))
                    type = this.String;
                else if (value instanceof Date)
                    type = this.Date;
                else if (isRegExp(value))
                    type = this.RegExp;
                else if (isArray(value))
                    type = this.Array;
                else if (isFunction(value))
                    type = this.Function;
                else if (isObject(value))
                    type = this.Object;
                else if (isConstructor(value))
                    type = this.Type;
                else if (value instanceof Object)
                    type = this.Instance;
                else if (isError(value))
                    type = this.Error;
                break;
        }

        return type as Nullable<U>;
    }
}

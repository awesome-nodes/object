import { ObjectBase } from 'object';


export interface IKeyValuePair<TKey, TValue>
{
    key: TKey;
    value: TValue;
}

export class KeyValuePair<TKey, TValue> extends ObjectBase implements IKeyValuePair<TKey, TValue>
{
    private readonly _key: TKey;
    private _value: TValue;

    //region Public Properties

    //tslint:disable-next-line:newspaper-order
    public get key(): TKey
    {
        return this._key;
    }

    public get value(): TValue
    {
        return this._value;
    }

    public set value(value: TValue)
    {
        this._value = value;
    }

    //endregion

    public constructor(key: TKey, value?: TValue)
    {
        super(String(key));
        this._key   = key;
        this._value = value as TValue;
    }

    public toString(): string
    {
        return `${super.toString()} -> ${this.value}`;
    }
}

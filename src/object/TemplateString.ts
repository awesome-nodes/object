import { PlainObject } from 'simplytyped';


export type TemplateItemIndexType = number | string;

export type TemplateStringValues = [] | PlainObject;

export interface ITemplateStringFunction<T extends TemplateItemIndexType>
{
    (...values: Array<TemplateStringValues>): string;

    Strings: TemplateStringsArray | ArrayLike<unknown>;
    Keys: Array<T>;

    Append<I extends TemplateItemIndexType>(
        string: TemplateString<I>, ...values: Array<TemplateStringValues>): TemplateString<I>;

    Prepend<I extends TemplateItemIndexType>(
        string: TemplateString<I>, ...values: Array<TemplateStringValues>): TemplateString<I>;
}

export type TemplateString<T extends TemplateItemIndexType = TemplateItemIndexType> =
    ITemplateStringFunction<T> | string;

/**
 * Tag-function for constructing string interpolation templates.
 * @param {TemplateStringsArray | ArrayLike<unknown>} strings
 * @param {number | string} keys
 * @returns {ITemplateStringFunction}
 * @example TemplateString\`This '${'value'}' can be replaced.\`({ value: 'newValue' });
 * @see https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/template_strings#Tagged_Template-Strings
 */
export function TemplateString<T extends TemplateItemIndexType = TemplateItemIndexType>(
    strings: TemplateStringsArray | ArrayLike<unknown>,
    ...keys: Array<T>): ITemplateStringFunction<T>
{
    const appendedTemplates: Array<TemplateString> = [];
    const prependTemplates: Array<TemplateString>  = [];
    let extraValues                                = {};

    return Object.assign(
        (...values: Array<TemplateStringValues>): string =>
        {
            const dict   = ExtendValues(values);
            const result = [strings instanceof Array ? strings[0] : strings] as Array<string>;

            keys.forEach((key: TemplateItemIndexType, i: number) =>
            {
                result.push(
                    typeof key == 'number' ? values[key] : dict[key],
                    strings[i + 1] as string,
                );
            });

            return prependTemplates
                .map(_prepend => typeof _prepend == 'function' ? _prepend(values) : _prepend)
                .concat(result)
                .concat(appendedTemplates.map(_append => typeof _append == 'function' ? _append(values) : _append))
                .join('');
        },
        {
            Strings: strings,
            Keys   : keys,
            Append,
            Prepend,
        },
    ) as ITemplateStringFunction<T>;

    function Append(
        this: ITemplateStringFunction<T>,
        templateString: TemplateString<T>, ...values: Array<TemplateStringValues>): TemplateString<T>
    {
        if (templateString.length) {
            appendedTemplates.push(templateString);
            extraValues = ExtendValues(values);
        }

        return this;
    }

    function Prepend(
        this: ITemplateStringFunction<T>,
        templateString: TemplateString<T>, ...values: Array<TemplateStringValues>): TemplateString<T>
    {
        if (templateString.length) {
            prependTemplates.push(templateString);
            extraValues = ExtendValues(values);
        }

        return this;
    }

    function ExtendValues(values: Array<TemplateStringValues>): PlainObject
    {
        return { ...extraValues, ...(values[values.length - 1] || {}) };
    }
}

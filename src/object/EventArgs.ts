import { ObjectBase } from 'object/ObjectBase';


/**
 * Represents a event data container used within the object event model,
 * and provides a value to use for events that do not include event data.
 */
export class EventArgs extends ObjectBase
{
    static get Empty(): EventArgs
    {
        return this._Empty;
    }

    private static readonly _Empty: EventArgs = new EventArgs();
}

/**
 * Defines a delegate signature used to invoke command handler delegates on raised events.
 */
export type IEventDelegate<TSender extends Object, TEventArgs extends EventArgs = EventArgs> =
    (delegate: IEvent<TSender, TEventArgs>, thisArg?: object) => void;

/**
 * Defines the delegate signature for the event model using the command pattern.
 */
export type IEvent<TSender extends Object, TEventArgs extends EventArgs = EventArgs> =
    (sender: TSender, eventArgs?: TEventArgs) => void | Promise<void>;

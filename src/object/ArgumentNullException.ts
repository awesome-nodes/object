import { ArgumentException } from 'object/ArgumentException';
import { Exception } from 'object/Exception';
import { TemplateString } from 'object/TemplateString';


export class ArgumentNullException extends ArgumentException
{
    public constructor(message: TemplateString<string>, paramName: string, innerException?: Exception)
    {
        super(
            TemplateString`Parameter '${'paramName'}' can not be neither 'null', 'undefined' or empty.`
                .Prepend(message),
            paramName,
            innerException,
        );
    }
}

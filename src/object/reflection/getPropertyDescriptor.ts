import { Nullable, PlainObject } from 'simplytyped';


/**
 * Returns all property names found withing the object prototype chain.
 * @param {PlainObject} obj The object in which to look for the properties.
 * @returns {Array<string>} A list of property names.
 */
export function getAllPropertyNames(obj: PlainObject): Array<string>
{
    const protoProps = new Map();

    do {
        const props = new Array<string>();
        protoProps.set(obj, props);
        Object.getOwnPropertyNames(obj).forEach(prop =>
        {
            if (!prop.endsWith('__') && props.indexOf(prop) === -1)
                props.push(prop);
        });

        obj = Object.getPrototypeOf(obj);
    } while (obj);

    return Array.from(protoProps.entries()).reverse().reduce(
        (current, next) =>
        {
            current.push(...next[1]);

            return current;
        },
        new Array<string>(),
    );
}

/**
 * Returns an object describing the configuration of a specific property on a given object
 * (that is, present on the object's prototype chain)
 * @param {PlainObject} obj The object in which to look for the property.
 * @param {string} prop The name or {@link Symbol} of the property whose description is to be retrieved.
 * @returns {Nullable<PropertyDescriptor>} A property descriptor of the given property if it exists on the object,
 * {@link undefined} otherwise
 */
export function getPropertyDescriptor(obj: PlainObject, prop: string): Nullable<PropertyDescriptor>
{
    let ownPropertyDescriptor: Nullable<PropertyDescriptor>;
    do {
        ownPropertyDescriptor = Object.getOwnPropertyDescriptor(obj, prop);
        if (ownPropertyDescriptor)
            break;

        obj = Object.getPrototypeOf(obj);
    } while (obj);

    return ownPropertyDescriptor;
}

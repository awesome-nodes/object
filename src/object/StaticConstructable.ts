import { ConstructorFunction } from 'simplytyped';


/**
 * Defines a statically constructable class.
 */
export interface IStaticConstructable<T extends object = object> extends ConstructorFunction<T>
{
    construct(): void;
}

/**
 * Class decorator for static constructable types.
 * @param {IStaticConstructable} constructor
 * @returns {IStaticConstructable}
 */
export function StaticConstructable<T extends IStaticConstructable = IStaticConstructable>(constructor: T): T
{
    constructor.construct();

    return constructor;
}

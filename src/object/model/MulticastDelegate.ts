import { KeyValuePair } from 'object/collections/KeyValuePair';
import { Delegate, EventArgs, IEvent } from 'object/model/Delegate';
import { IDelegate } from 'object/model/IDelegate';
import { Observable, Observer, Subscription, throwError } from 'rxjs';
import { catchError, publish, refCount } from 'rxjs/operators';


/**
 * Interface definition of MulticastDelegate for deriving/implementing classes.
 */
//eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IMulticastDelegate<TSender extends object, TEventArgs extends EventArgs>
    extends IDelegate<TSender, TEventArgs>
{
}

export class HandlerSubscription<TSender extends object, TEventArgs extends EventArgs>
    extends KeyValuePair<IEvent<TSender, TEventArgs>, Subscription>
{
    public get Handler(): IEvent<TSender, TEventArgs>
    {
        return this.key;
    }

    public get Subscription(): Subscription
    {
        return this.value;
    }
}

/**
 * Represents a delegate which supports synchronous and asynchronous invocation of multiple handlers.
 */
//tslint:disable-next-line:max-classes-per-file
export class MulticastDelegate<TSender extends object, TEventArgs extends EventArgs>
    extends Delegate<TSender, TEventArgs> implements IMulticastDelegate<TSender, TEventArgs>
{
    private _Handlers: Array<HandlerSubscription<TSender, TEventArgs>> = [];

    public get Handlers(): Array<HandlerSubscription<TSender, TEventArgs>>
    {
        return this._Handlers;
    }

    /** @inheritDoc */
    public Subscribe(handler: IEvent<TSender, TEventArgs>, thisArg?: object): Subscription
    {
        if (thisArg != null)
            handler = handler.bind(thisArg);

        if (this._Observable == null) {
            // The producer is this delegate instance, so we create and store a hot observable.
            const observable = new Observable((observer: Observer<TEventArgs>) =>
            {
                this._Observer = observer;

                return (): void =>
                {
                    this._Observable   = undefined;
                    this._Subscription = undefined;
                    this._Observer     = undefined;
                };
            });
            this._Observable = MulticastDelegate.Multicast(observable);
        }

        // Create reference counted subscription
        const subscription = this._Observable.subscribe((eventArgs: TEventArgs) =>
        {
            handler(this._Target, (eventArgs ? eventArgs : EventArgs.Empty as TEventArgs));
        });

        // Remove the subscription from the handler list when unsubscribed.
        subscription.add(() =>
        {
            this.Unsubscribe(handler);
        });

        // Store the event subscription for multicasting.
        const handlerSubscription = new HandlerSubscription(handler, subscription);
        this._Handlers.push(handlerSubscription);

        return handlerSubscription.Subscription;
    }

    /** @inheritDoc */
    public Unsubscribe(handler: IEvent<TSender, TEventArgs>): void
    {
        this._Handlers = this._Handlers.filter(subscriber =>
        {
            const match = subscriber.Handler === handler;
            if (match && !subscriber.Subscription.closed)
                subscriber.Subscription.unsubscribe();

            return !match;
        });
    }

    /**
     *
     * @param observable
     */
    public static Multicast<T>(observable: Observable<T>): Observable<T>
    {
        return observable.pipe(
            catchError((err: Error) => throwError(err)),
            publish(),
            refCount(),
        );
    }
}

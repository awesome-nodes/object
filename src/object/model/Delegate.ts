import { EventArgs, Exception, IEvent, ObjectBase } from 'object';
import { IDelegate } from 'object/model/IDelegate';
import { NextObserver, Observable, Observer, Subscription } from 'rxjs';
import { Nullable } from 'simplytyped';


export * from 'object/EventArgs';

/* eslint-enable max-len */

/**
 * Represents a delegate which supports synchronous and asynchronous invocation of only one handler at a time.
 * @see https://github.com/Reactive-Extensions/RxJS#about-the-reactive-extensions
 */
export class Delegate<TSender extends object, TEventArgs extends EventArgs>
    extends ObjectBase implements IDelegate<TSender, TEventArgs>
{
    private _Handler?: IEvent<TSender, TEventArgs>;

    private _PreventSubscribe = false;

    protected _Observable: Nullable<Observable<TEventArgs>>;

    protected _Subscription: Nullable<Subscription>;

    protected _Observer: Nullable<NextObserver<TEventArgs>>;

    //tslint:disable-next-line:newspaper-order
    public get Target(): TSender
    {
        return this._Target;
    }

    public get Handler(): IEvent<TSender, TEventArgs>
    {
        return this._Handler!;
    }

    /**
     * Indicates if event subscriptions should fail in case when an event consumer is already present.
     * @returns {boolean}
     */
    public get PreventSubscribe(): boolean
    {
        return this._PreventSubscribe;
    }

    /**
     * Sets the value for Delegate.PreventSubscribe
     * @param value
     */
    public set PreventSubscribe(value: boolean)
    {
        this._PreventSubscribe = value;
    }

    /**
     * Initializes this instance.
     * @param {TSender} _Target
     */
    constructor(protected _Target: TSender)
    {
        super();
    }

    /** @inheritDoc */
    public Subscribe(handler: IEvent<TSender, TEventArgs>, thisArg?: object): Subscription
    {
        // Break out on invalid object state or implementation failure.
        if (typeof handler !== 'function')
            throw new Exception(`WebCore: A event subscription requires a handler function. Provided Value:${handler}`);

        //eslint-disable-next-line curly
        if (this._PreventSubscribe && this._Handler)
            throw new Exception(`WebCore: The handler '${handler.prototype.constructor.name
            }' cannot be registered. Only one event subscriber can be registered at a time.`);

        // Dispose existing subscription
        if (this._Subscription != null)
            this._Subscription.unsubscribe();

        if (thisArg != null)
            handler = handler.bind(thisArg);

        this._Handler = handler;

        // The producer is this delegate instance, so we create and store a hot observable.
        this._Observable = new Observable((observer: Observer<TEventArgs>) =>
        {
            this._Observer = observer;

            return (): void =>
            {
                this._Handler      = undefined;
                this._Observable   = undefined;
                this._Subscription = undefined;
                this._Observer     = undefined;
            };
        });

        // Create, store and return the event subscription
        return this._Subscription = this._Observable.subscribe((eventArgs: TEventArgs) =>
        {
            this._Handler && this._Handler(this._Target, (eventArgs ? eventArgs : EventArgs.Empty as TEventArgs));
        });
    }

    /** @inheritDoc */
    public Unsubscribe(handler: IEvent<TSender, TEventArgs>): void
    {
        if (this._Handler == handler && this._Subscription != null)
            this._Subscription.unsubscribe();
    }

    /** @inheritDoc */
    public Invoke(eventArgs?: TEventArgs): void
    {
        if (this._Observer)
            this._Observer.next(eventArgs ? eventArgs : EventArgs.Empty as TEventArgs);
    }

    /** @inheritDoc */
    public async InvokeAsync(eventArgs?: TEventArgs): Promise<TEventArgs>
    {
        // Create initially an asynchronous deferred event and
        // attach the observable producer invocation on top of the promise event chain.
        return new Promise<TEventArgs>((resolve) =>
        {
            // Resolve the deferred immediately
            // (Gets called after the current thread context has been processed
            // and all other promise handlers have been attached to the invocation)
            resolve(eventArgs as TEventArgs);
        }).then(() =>
        {
            this.Invoke(eventArgs);

            return eventArgs;
        }) as Promise<TEventArgs>;
    }
}

import { toJSON } from 'object/data/JSON';
import { PropertyChangedEventArgs } from 'object/model/PropertyChangedEventArgs';
import { Nullable, PlainObject, StringKeys } from 'simplytyped';


export class PropertyObservedEventArgs<T extends object> extends PropertyChangedEventArgs<T>
{
    public get OldValue(): unknown
    {
        return this._OldValue;
    }

    public get NewValue(): unknown
    {
        return this._NewValue;
    }

    public get PropertyChanged(): Nullable<PropertyChangedEventArgs<unknown>>
    {
        return this._PropertyChanged;
    }

    public toJSON: <TJson = this>(...includeProperties: Array<StringKeys<TJson>>) => PlainObject;

    public constructor(
        propertyName: StringKeys<T>,
        private readonly _OldValue: unknown,
        private readonly _NewValue: unknown,
        private readonly _PropertyChanged?: PropertyChangedEventArgs<any>)
    {
        super(propertyName);

        this.toJSON = toJSON;
    }
}

/* eslint-disable max-len */
import { EventArgs } from 'object';
import { IDelegate } from 'object/model/IDelegate';
import { MulticastDelegate } from 'object/model/MulticastDelegate';
import { ObservableCollectionEntry } from 'object/model/ObservableCollectionEntry';
import {
    ObservableCollectionEntryEventArgs,
    ObservableEntryEventDelegate,
} from 'object/model/ObservableCollectionEntryEventArgs';
import { PropertyChangedEventArgs } from 'object/model/PropertyChangedEventArgs';
import { PropertyObservable } from 'object/model/PropertyObservable';
import { PropertyObservedEventArgs } from 'object/model/PropertyObservedEventArgs';
import { ConstructorFor } from 'simplytyped';


export class ObservableCollection<TItem extends PropertyObservable<TItem>,
    TEntry extends ObservableCollectionEntry<TEntry, TItem, TObservable>,
    TObservable extends ObservableCollection<TItem, TEntry, TObservable>>
    extends PropertyObservable<TObservable>
{
    protected readonly _Entries = new Array<TEntry>();

    public readonly EntryAdded: ObservableEntryEventDelegate<TItem, TEntry, TObservable>;
    public readonly EntryChanged: ObservableEntryEventDelegate<TItem, TEntry, TObservable>;
    public readonly EntryRemoved: ObservableEntryEventDelegate<TItem, TEntry, TObservable>;

    public get Count()
    {
        return this._Entries.length;
    }

    public get Entries(): Array<TEntry>
    {
        return this._Entries;
    }

    public get Items(): Array<TItem>
    {
        return this._Entries.reduce((items, entry) =>
        {
            items.push(entry.Item);

            return items;
        }, new Array<TItem>());
    }

    public constructor(
        eventDelegate:
            ConstructorFor<IDelegate<object, EventArgs>> = MulticastDelegate,
        name?: string,
    )
    {
        super(eventDelegate, name);

        this.EntryAdded   = new eventDelegate(this) as ObservableEntryEventDelegate<TItem, TEntry, TObservable>;
        this.EntryChanged = new eventDelegate(this) as ObservableEntryEventDelegate<TItem, TEntry, TObservable>;
        this.EntryRemoved = new eventDelegate(this) as ObservableEntryEventDelegate<TItem, TEntry, TObservable>;
    }

    protected OnEntryAdded(sender: this, ea: ObservableCollectionEntryEventArgs<TEntry>): void
    {
        ea.Entry.PropertyChanged.Subscribe(this.OnEntryPropertyChanged, this);
        this.EntryAdded.Invoke(ea);
    }

    protected OnEntryChanged(sender: this, ea: ObservableCollectionEntryEventArgs<TEntry>): void
    {
        this.EntryChanged.Invoke(ea);
    }

    protected OnEntryPropertyChanged(
        sender: TEntry,
        ea?: PropertyObservedEventArgs<TEntry>): void
    {
        this.OnEntryChanged(this, new ObservableCollectionEntryEventArgs(sender, ea));

        this.OnReferenceObserved('Entries', this._Entries);
    }

    protected OnEntryItemPropertyChanged(
        sender: TItem,
        ea?: PropertyChangedEventArgs<TItem>): void
    {
        const entry = this._Entries.find(_entry => _entry.Item == sender);

        this.OnEntryPropertyChanged(
            entry as TEntry,
            //eslint-disable-next-line @typescript-eslint/ban-ts-comment
            //@ts-ignore
            new PropertyObservedEventArgs<TEntry>('Item', entry.Item, entry.Item, ea),
        );
    }

    protected OnEntryRemoved(sender: this, ea: ObservableCollectionEntryEventArgs<TEntry>): void
    {
        ea.Entry.PropertyChanged.Unsubscribe(this.OnEntryPropertyChanged);
        ea.Entry.Item.PropertyChanged.Unsubscribe(this.OnEntryItemPropertyChanged);
        this.EntryRemoved.Invoke(ea);
    }

    protected AddEntry(...entries: Array<TEntry>): void
    {
        entries.forEach(entry => entry.Item.PropertyChanged.Subscribe(this.OnEntryItemPropertyChanged, this));

        this._Entries.push(...entries);

        this.OnEntriesChanged && this.OnEntriesChanged();

        entries.forEach(entry =>
        {
            this.OnEntryAdded(this, new ObservableCollectionEntryEventArgs<TEntry>(entry));
        });

        this.OnReferenceObserved('Entries', this._Entries);
    }

    protected CreateEntry(item: TItem): TEntry
    {
        const entry = new ObservableCollectionEntry<TEntry, TItem, TObservable>(
            this as unknown as TObservable,
            item,
            this._EventDelegate,
        );

        return entry as TEntry;
    }

    protected OnEntriesChanged?(): void;

    public Add(item: TItem): TEntry
    {
        const entry = this.CreateEntry(item);

        this.AddEntry(entry);

        return entry;
    }

    public AddRange(...items: Array<TItem>): Array<TEntry>
    {
        const entries = new Array<TEntry>();

        items.forEach(item =>
        {
            const entry = this.CreateEntry(item);
            if (entry)
                entries.push(entry);
        });

        this.AddEntry(...entries);

        return entries;
    }

    public Remove(entry: TEntry): void
    {
        this._Entries.splice(this._Entries.indexOf(entry), 1);

        this.OnEntriesChanged && this.OnEntriesChanged();

        this.OnEntryRemoved(this, new ObservableCollectionEntryEventArgs(entry));

        this.OnReferenceObserved('Entries', this._Entries);
    }
}

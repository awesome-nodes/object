import { EventArgs, IEquatable, ObjectBase } from 'object';
import { ObjectBase as DataObjectBase } from 'object/data/ObjectBase';
import { IDelegate } from 'object/model/IDelegate';
import { INotifyPropertyChanged } from 'object/model/INotifyPropertyChanged';
import { MulticastDelegate } from 'object/model/MulticastDelegate';
import { PropertyChangedEventArgs, PropertyChangedEventDelegate } from 'object/model/PropertyChangedEventArgs';
import { PropertyObservedEventArgs } from 'object/model/PropertyObservedEventArgs';
import { ConstructorFor, PlainObject, StringKeys } from 'simplytyped';


export class PropertyObservable<T extends object> extends DataObjectBase implements INotifyPropertyChanged<T>
{
    public readonly PropertyChanged: PropertyChangedEventDelegate<T, PropertyObservedEventArgs<T>>;

    constructor(
        protected readonly _EventDelegate: ConstructorFor<IDelegate<object, EventArgs>> = MulticastDelegate,
        name?: string,
    )
    {
        super(name);

        this.PropertyChanged =
            new _EventDelegate(this) as PropertyChangedEventDelegate<T, PropertyObservedEventArgs<T>>;
    }

    protected SetValue(propertyName: StringKeys<T>, value: symbol | string | number | {} | IEquatable<ObjectBase>)
        : boolean
    {
        const backingFieldName = `_${propertyName}`;
        const fieldValue       = (this as PlainObject)[backingFieldName];
        if (fieldValue == value || fieldValue != null
            && ((typeof fieldValue == 'object') && 'Equals' in fieldValue && fieldValue.Equals(value)))
            return false;

        (this as PlainObject)[backingFieldName] = value;

        this.OnPropertyChanged(this, new PropertyObservedEventArgs(propertyName, fieldValue, value));

        return true;
    }

    protected OnReferenceObserved<This = this>(
        propertyName: StringKeys<T | This>,
        reference: unknown,
        ea?: PropertyChangedEventArgs<any>): void
    {
        this.OnPropertyChanged(
            this,
            new PropertyObservedEventArgs<T>(propertyName as StringKeys<T>, reference, reference, ea),
        );
    }

    protected OnValueObserved<This = this>(
        propertyName: StringKeys<T | This>,
        oldValue: unknown,
        newValue: unknown,
        ea?: PropertyChangedEventArgs<any>): void
    {
        this.OnPropertyChanged(
            this,
            new PropertyObservedEventArgs<T>(propertyName as StringKeys<T>, oldValue, newValue, ea),
        );
    }

    protected OnPropertyChanged(sender: PropertyObservable<T>, ea: PropertyObservedEventArgs<T>): void
    {
        this.PropertyChanged.Invoke(ea);
    }
}

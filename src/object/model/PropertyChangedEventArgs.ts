import { EventArgs } from 'object';
import { IDelegate } from 'object/model/IDelegate';
import { StringKeys } from 'simplytyped';


export class PropertyChangedEventArgs<T> extends EventArgs
{
    public get PropertyName(): StringKeys<T>
    {
        return this._PropertyName;
    }

    public constructor(private readonly _PropertyName: StringKeys<T>)
    {
        super();
    }
}

export type PropertyChangedEventDelegate<TSender extends object,
    TEventArgs extends PropertyChangedEventArgs<TSender>
        = PropertyChangedEventArgs<TSender>> = IDelegate<TSender, TEventArgs>;

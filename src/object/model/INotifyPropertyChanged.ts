import { IDelegate } from 'object/model/IDelegate';
import { PropertyChangedEventArgs } from 'object/model/PropertyChangedEventArgs';


export interface INotifyPropertyChanged<T extends object>
{
    PropertyChanged: IDelegate<T, PropertyChangedEventArgs<T>>;
}

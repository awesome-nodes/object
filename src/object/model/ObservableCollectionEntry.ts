import { EventArgs } from 'object';
import { IDelegate } from 'object/model/IDelegate';
import { MulticastDelegate } from 'object/model/MulticastDelegate';
import { ObservableCollection } from 'object/model/ObservableCollection';
import { PropertyObservable } from 'object/model/PropertyObservable';
import { PropertyObservedEventArgs } from 'object/model/PropertyObservedEventArgs';
import { ConstructorFor, Nullable, PlainObject, StringKeys } from 'simplytyped';


export class ObservableCollectionEntry<TEntry extends ObservableCollectionEntry<TEntry, TItem, TCollection>,
    TItem extends PropertyObservable<TItem>,
    TCollection extends ObservableCollection<TItem, TEntry, TCollection>>
    extends PropertyObservable<TEntry>
{
    protected readonly _Collection: TCollection;
    protected readonly _Item: TItem;

    public get Collection(): TCollection
    {
        return this._Collection;
    }

    public get Item(): TItem
    {
        return this._Item;
    }

    public get Index(): number
    {
        return this._Collection.Entries.indexOf(this as unknown as TEntry);
    }

    public get Previous(): Nullable<TEntry>
    {
        const index = this.Index;

        return index > 0
            ? this._Collection.Entries[index - 1]
            : null;
    }

    public get Next(): Nullable<TEntry>
    {
        const index = this.Index;

        return index < this._Collection.Entries.length - 1
            ? this._Collection.Entries[index + 1]
            : null;
    }

    public constructor(
        collection: TCollection,
        item: TItem,
        eventDelegate: ConstructorFor<IDelegate<object, EventArgs>> = MulticastDelegate,
        name?: string,
    )
    {
        super(eventDelegate, name);

        this._Collection = collection;
        this._Item       = item;

        this._Item.PropertyChanged.Subscribe(this.OnItemPropertyChanged, this);
    }

    protected OnItemPropertyChanged(sender: TItem, ea?: PropertyObservedEventArgs<TItem>): void
    {
        this.OnReferenceObserved<ObservableCollectionEntry<TEntry, TItem, TCollection>>('Item', sender, ea);
    }

    public toJSON<T = this>(...includeProperties: Array<StringKeys<this | T>>): PlainObject
    {
        return super.toJSON(
            'Item',
            'Index',
            ...includeProperties as Array<StringKeys<PropertyObservable<TEntry>>>,
        );
    }
}

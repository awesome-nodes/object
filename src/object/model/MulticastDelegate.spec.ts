/* tslint:disable:new-parens no-unused-expression member-access */
import { itShould } from '@awesome-nodes/unittest';
import { EventArgs } from 'object/model/Delegate';
import { DelegateTest } from 'object/model/DelegateTest';
import { MulticastDelegate } from 'object/model/MulticastDelegate';


new class MulticastDelegateTest extends DelegateTest
{
    //eslint-disable-next-line @typescript-eslint/ban-ts-comment
    //@ts-ignore
    protected Delegate?: MulticastDelegate<MulticastDelegateTest, EventArgs>;

    BeforeEach(): void
    {
        this.Delegate = new MulticastDelegate<MulticastDelegateTest, EventArgs>(this);
    }

    Test(): void
    {
        itShould('receive data from event subscriptions', () =>
        {
            this.InvokeTest();
        });

        itShould('receive data from asynchronous event subscriptions', (done: jest.DoneCallback) =>
        {
            const subscription = this.AsyncTest();
            subscription.add(done);
            setTimeout(() =>
            {
                subscription.unsubscribe();
            });
        });

        itShould('unsubscribe from event subscriptions', () =>
        {

            // Subscription 1
            const subscription1 = this.InvokeTest();
            expect(this.Delegate!.Handlers.length).toBe(1);

            // Subscription 2
            const subscription2 = this.InvokeTest();
            expect(this.Delegate!.Handlers.length).toBe(2);

            // Subscription 3
            const subscription3 = this.InvokeTest();
            expect(this.Delegate!.Handlers.length).toBe(3);

            // Verify detach of subscription
            subscription1.unsubscribe();
            expect(this.Delegate!.Handlers.length).toBe(2);
            subscription2.unsubscribe();
            expect(this.Delegate!.Handlers.length).toBe(1);
            subscription3.unsubscribe();
            expect(this.Delegate!.Handlers.length).toBe(0);
        });
    }
};

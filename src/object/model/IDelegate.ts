/* tslint:disable:max-line-length */
/* eslint-disable max-len */
import { EventArgs, IEvent } from 'object';


/**
 * Interface definition of Delegate for deriving/implementing classes.
 */
export interface IDelegate<TSender extends Object, TEventArgs extends EventArgs>
{
    /**
     * Registers a new event subscription handler with this delegate instance. Note: If Delegate.PreventSubscribe is set
     * to 'false' then the new specified event subscription will dispose and override the existing one.
     * @param {IEvent} handler The event handler function. Note It is possible to object.bind the handler owner.
     * @param {object} thisArg An object to which the this keyword can refer in the callbackfn function. If thisArg is omitted, undefined is used as the this value.
     * @returns {Subscription} The observable subscription which can be disposed through calling observable.unsubscribe().
     */
    Subscribe(handler: IEvent<TSender, TEventArgs>, thisArg?: object): void;

    /**
     * Unsubscribe the specified handler from this delegate instance.
     * @param {IEvent} handler The event handler function.
     */
    Unsubscribe(handler: IEvent<TSender, TEventArgs>): void;

    /**
     * Invokes this delegate with optionally specifiyable event args.
     * @param {TEventArgs} eventArgs The arguments container object for this event. If 'null' then {EventArgs.Empty} will be used as default value.
     */
    Invoke(eventArgs?: TEventArgs): void;

    /**
     * Invokes the handler subscriptions of this delegate instance asynchronously.
     * @param {TEventArgs} eventArgs The arguments container object for this event. If 'null' the {EventArgs.Empty} will be used as per default.
     * @returns {Promise<TEventArgs extends EventArgs>} A promise which will be triggered when the event handler subscription havs been processed.
     */
    InvokeAsync?(eventArgs?: TEventArgs): Promise<TEventArgs>;
}

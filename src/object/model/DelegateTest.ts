/* eslint-disable @typescript-eslint/ban-types */
/* tslint:disable:newspaper-order member-access member-ordering */
import { itShould, TestCase } from '@awesome-nodes/unittest';
import { IEvent } from 'object';
import { Delegate, EventArgs } from 'object/model/Delegate';
import { Subscription } from 'rxjs';


export class DelegateTest extends TestCase
{
    protected Delegate?: Delegate<DelegateTest, EventArgs>;

    BeforeEach(): void
    {
        this.Delegate = new Delegate<DelegateTest, EventArgs>(this);
    }

    Test(): void
    {
        itShould('receive data from event subscriptions', () =>
        {
            this.InvokeTest();
        });

        itShould('unsubscribe from event subscriptions', () =>
        {
            const subscription = this.InvokeTest();
            expect(this.Delegate!.Handler).toBeDefined();
            subscription.unsubscribe();
            expect(this.Delegate!.Handler).toBeUndefined();
        });
    }

    /**
     * Subscribes a new event observer handler and expects existing event data.
     * @param {IEvent<ObjectBase, EventArgs>} eventObserver
     * @returns {Subscription} The RxJS subscription.
     */
    private SubscriptionTest(eventObserver?: IEvent<object, EventArgs>): Subscription
    {
        return this.Delegate!.Subscribe(eventObserver || ((sender: object, ea?: EventArgs) =>
        {
            expect(ea).toBeDefined();
        }));
    }

    InvokeTest(eventObserver?: IEvent<object, EventArgs>): Subscription
    {
        const subscription = this.SubscriptionTest(eventObserver);
        this.Delegate!.Invoke(EventArgs.Empty);

        return subscription;
    }

    AsyncTest(eventObserver?: IEvent<object, EventArgs>): Subscription
    {
        const subscription = this.SubscriptionTest(eventObserver);
        this.Delegate!.InvokeAsync(EventArgs.Empty);

        return subscription;
    }
}

import { EventArgs } from 'object';
import { toJSON } from 'object/data/JSON';
import { IDelegate as IDelegateBase } from 'object/model/IDelegate';
import { ObservableCollection } from 'object/model/ObservableCollection';
import { ObservableCollectionEntry } from 'object/model/ObservableCollectionEntry';
import { PropertyObservable } from 'object/model/PropertyObservable';
import { PropertyObservedEventArgs } from 'object/model/PropertyObservedEventArgs';
import { Nullable, PlainObject, StringKeys } from 'simplytyped';


export class ObservableCollectionEntryEventArgs<TEntry extends object> extends EventArgs
{
    public get Entry(): TEntry
    {
        return this._Entry;
    }

    public get PropertyObserved(): Nullable<PropertyObservedEventArgs<TEntry>>
    {
        return this._PropertyObserved;
    }

    public toJSON: <TJson = this>(...includeProperties: Array<StringKeys<TJson>>) => PlainObject;

    public constructor(
        private readonly _Entry: TEntry,
        private readonly _PropertyObserved?: PropertyObservedEventArgs<TEntry>)
    {
        super();

        this.toJSON = toJSON;
    }
}

export type ObservableEntryEventDelegate<TItem extends PropertyObservable<TItem>,
    TEntry extends ObservableCollectionEntry<TEntry, TItem, TObservable>,
    TObservable extends ObservableCollection<TItem, TEntry, TObservable>>
    = IDelegateBase<ObservableCollection<TItem, TEntry, TObservable>, ObservableCollectionEntryEventArgs<TEntry>>;

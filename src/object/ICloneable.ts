/**
 * Supports cloning, which creates a new instance of a class with the same value as an existing instance.
 * @see https://docs.microsoft.com/en-us/dotnet/api/system.icloneable
 */
export interface ICloneable<T extends object>
{
    /**
     * Creates a new object that is a copy of the current instance.
     * @returns {T} A new object that is a copy of this instance.
     */
    Clone(): T;
}

/**
 * Determines if the provided object implements the {@link IEquatable} interface.
 * @param {object} instance
 * @returns {instance is ICloneable<T>} `true` if the current object is cloneable; otherwise, `false`.
 */
export function isCloneable<T extends object>(instance: object): instance is ICloneable<T>
{
    return 'Clone' in instance;
}

/**
 * Defines an abstract object body
 */
//tslint:disable-next-line:ban-types
export type AbstractClass<T> = Function & { prototype: T };

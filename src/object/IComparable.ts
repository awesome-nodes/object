import { ObjectBase } from 'object/ObjectBase';


/**
 * Defines a generalized comparison method that a value type or class implements
 * to create a type-specific comparison method for ordering or sorting its instances.
 */
export interface IComparable<T extends ObjectBase>
{
    CompareTo(other: T): number;
}
